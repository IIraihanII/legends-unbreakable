﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Character : LivingEntity
{
    Player player;

    CharacterAnimator characterAnimator;

    public enum Class
    {
        Warrior,
        Archer,
        Caster,
    }

    public Projectile[] projectiles;
    public SpawnedParticle healEffect;
    public SpawnedParticle teleporterEffect;
    public SpawnedParticle deathEffect;

    public Class @class;

    void Start()
    {
    }

    void Update ()
    {
    }

    public void SetCharacterAttributes()
    {
        player = GetComponentInParent<Player>();

        characterAnimator = GetComponentInChildren<CharacterAnimator>();

        switch (@class)
        {
            case Class.Warrior:
                player.maxHP = maxHP;
                player.maxMP = maxMP;

                player.skills[0].SetSkill(Skill.SkillType.Offensive, "Slash", 2, 10, 10, 3);
                player.skills[1].SetSkill(Skill.SkillType.Offensive, "Sheild Strike", 3, 12, 10, 3);
                player.skills[2].SetSkill(Skill.SkillType.Heal, "Recover", 5, 20, 20, 0);
                break;
            case Class.Archer:
                player.maxHP = maxHP;
                player.maxMP = maxMP;

                player.skills[0].SetSkill(Skill.SkillType.Offensive, "Arrow", 2, 8, 10, 6);
                player.skills[1].SetSkill(Skill.SkillType.Offensive, "Power Shot", 3, 15, 15, 6);
                player.skills[2].SetSkill(Skill.SkillType.Heal, "Recover", 5, 20, 20, 0);

                break;
            case Class.Caster:
                player.maxHP = maxHP;
                player.maxMP = maxMP;

                player.skills[0].SetSkill(Skill.SkillType.Offensive, "Bolt", 3, 20, 20, 4);
                player.skills[1].SetSkill(Skill.SkillType.Offensive, "Greater Bolt", 5, 30, 25, 4);
                player.skills[2].SetSkill(Skill.SkillType.Multi, "Homing Bolt", 5, 15, 25, 0);
                break;
        }

        player.Reset();
    }

    public void SkillEffect(int skillNumber, Skill.SkillType skillType, Player[] targets)
    {

        if (skillType == Skill.SkillType.Offensive)
        {
            Projectile projectile = Instantiate(projectiles[skillNumber], transform.position, transform.rotation) as Projectile;
            projectile.SetTarget(targets[0].transform.position);
        }
        else if (skillType == Skill.SkillType.Multi)
        {
            foreach (Player player in targets)
            {
                Projectile projectile = Instantiate(projectiles[skillNumber], transform.position, transform.rotation) as Projectile;
                projectile.SetTarget(player.transform.position);
            }
        }
        else
        {
            SpawnedParticle spawnedParticle = Instantiate(healEffect, transform.position, transform.rotation) as SpawnedParticle;
        }
    }

    public void DeathEffect()
    {
        SpawnedParticle spawnedParticle = Instantiate(deathEffect, transform.position, transform.rotation) as SpawnedParticle;
    }
    public void GainEffect()
    {
        SpawnedParticle spawnedParticle = Instantiate(teleporterEffect, transform.position, transform.rotation) as SpawnedParticle;
    }
}
