﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Map : MonoBehaviour
{
    [SerializeField]
    private List<Player> consistingPlayers = new List<Player>(0);
    [SerializeField]
    private Tile[] tiles;
    [SerializeField]
    private Tile checkPointTile;
    [SerializeField]
    private Tile winTile;
    public int mapNumber;

    //Getters//
    public string GetMapName()
    {
        return gameObject.name;
    }

    public Tile GetTile(int _tileNumber)
    {
        return tiles[_tileNumber];
    }

    void Awake()
    {
        for (int i = 0; i < tiles.Length; i++)
            tiles[i].tileIndex = i;        
    }

    void Update()
    {

    }

    public void AddPlayerToMap(Player player)
    {
        consistingPlayers.Add(player);

        Debug.Log("Player has been added");

        player.TeleportMove(GetComponent<Map>(), tiles[0]);
        player.StartNavMesh();
    }

    public void MovePlayer(Player player, int moveValue)
    {
        for (int i = 0; i < consistingPlayers.Count; i++)
        {
            if (consistingPlayers[i] == player)
            {
                int newTileNumber = player.tile.tileIndex + moveValue;

                if (newTileNumber >= tiles.Length)
                {
                    if (winTile != null)
                    {
                        player.Win();
                    }
                    MovePlayerToCheckPoint(player);
                }
                else
                    player.MoveTo(GetComponent<Map>(),tiles[newTileNumber]);
                break;
            }
        }
    }

    public void MovePlayerToCheckPoint(Player player)
    {
        RemovePlayerFromMap(player);
        player.MoveTo(GetComponent<Map>(), checkPointTile);
    }

    public void RemovePlayerFromMap(Player player)
    {
        consistingPlayers.Remove(player);
    }

    //public void AddPlayerToMap(Player player, int tileNumber, int restPosNumber)
    //{
    //    Vector3 targetPos = tiles[tileNumber].restPosition[restPosNumber].position;

    //    Debug.Log(targetPos);

    //    character.MoveToPosition(new Vector3(targetPos.x, 0.3f, targetPos.y)); 
    //}
}
