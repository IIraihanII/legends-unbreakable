﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    float speed = 10;
    Vector3 target;

    public void SetSpeed(float newSpeed)
    {
        speed = newSpeed;
    }

    public void SetTarget(Vector3 target)
    {
        this.target = target;
    }

	void Update ()
    {
        transform.position = Vector3.MoveTowards(transform.position, target, speed * Time.deltaTime);

        if (transform.position == target)
        {
            Destroy(gameObject);
        }
	}
}
