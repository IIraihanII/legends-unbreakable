﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour
{
    public enum TileType
    {
        Regular,
        CheckPoint,
        Win
    }

    public Transform[] restPosition;

    public int restingEntities;

    public int tileIndex;
    public TileType tileType;

    public Map[] transitionMaps;

    public int gainGemValue;

    public int loseGemValue;

    //Enemy object for encounter.

	void Start ()
    {
        restingEntities = -1;
	}
	
	void Update ()
    {
		
	}

    public Transform RestAt()
    {
        restingEntities++;

        Debug.Log("Rest pos for return");

        return restPosition[restingEntities];
    }

    public void Leave()
    {
        restingEntities--;

        if (restingEntities == -2)
        {
            restingEntities = -1;
        }
    }
}
