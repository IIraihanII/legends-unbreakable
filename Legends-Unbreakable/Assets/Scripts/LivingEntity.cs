﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LivingEntity : MonoBehaviour
{
    private int HP;
    private int MP;
    public int armour = 0;


    public int maxHP;
    public int maxMP;
    public int expirience;

    public int GetHP() { return HP; }
    public int GetMP() { return MP; }
    public int GetEXP() { return expirience; }

    public void SetHP(int HP) { this.HP = HP; }
    public void SetMP(int MP) { this.HP = MP; }
    public void AddEXP(int zenny) { this.expirience += zenny; }
    public void ReduceMP(int MP) { this.MP -= MP; }
    public void ResetEXP() { expirience = 0; }

    public void Reset()
    {
        HP = maxHP;
        MP = maxMP;
    }

    public void Heal(int value)
    {
        HP += value;

        if (HP > maxHP)
        {
            HP = maxHP;
        }
    }

    public void AddArmour(int value)
    {
        armour += value;
    }

    public void TakeDamage(int value)
    {
        HP  -= value;
    }
}
