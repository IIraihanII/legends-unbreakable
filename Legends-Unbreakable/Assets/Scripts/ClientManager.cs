﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;

public class ClientManager : MonoBehaviour
{
    //Used to decidie how the client behaves
    public enum NetworkState
    {
        Searching,
        Lobby,
        InGame
    }

    //Network attributes.
    NetworkClient client;
    public NetworkState networkState;

    //Reference to the network player controller
    NetworkPlayerController playerController;

    //Check if client is connected.
    public bool GetConnectionStatus() { return client.isConnected; }

    //GUI drawn to check stats for test purposes.
    private void OnGUI()
    {
        string ipaddress = Network.player.ipAddress;
        GUI.Box(new Rect(10, Screen.height - 50, 100, 50), ipaddress);
        GUI.Label(new Rect(20, Screen.height - 30, 100, 20), "Status: " + client.isConnected);
    }

    void Awake()
    {
        networkState = NetworkState.Searching;

        DontDestroyOnLoad(gameObject);
        client = new NetworkClient();

        //Handle funtions for the client manager.
        client.RegisterHandler(555, ChangeCanvas);
        client.RegisterHandler(666, BecomeActive);
        client.RegisterHandler(444, GetAvailableCharacters);
        client.RegisterHandler(777, GetPlayerPrimaryInfo);
        client.RegisterHandler(888, GetPlayerGameInfo);
        client.RegisterHandler(999, GetCheckPointOptions);
        client.RegisterHandler(333, GetPlayerSkillInfos);
        client.RegisterHandler(222, GetTargetPlayers);
    }

    private void Start()
    {
        playerController = GetComponent<NetworkPlayerController>();
    }

    //Function to connect to the server.
    public void ConnectToServer(string ipAddress)
    {
        client.Connect(ipAddress, 25000);
    }

    //Function send playyer name to the server.
    public void SendPlayerName(string playerName)
    {
        networkState = NetworkState.Lobby;

        StringMessage msg = new StringMessage();
        msg.value = playerName + "|" + Network.player.ipAddress;
        client.Send(111, msg);
    }

    //Function to change the canvas shown by the network player controller.
    public void ChangeCanvas(NetworkMessage networkMessage)
    {
        StringMessage msg = new StringMessage
        {
            value = networkMessage.ReadMessage<StringMessage>().value
        };

        playerController.ChangeCanvas(msg.value);
    }

    //Function to become active in the network player controller.
    public void BecomeActive(NetworkMessage networkMessage)
    {
        StringMessage msg = new StringMessage
        {
            value = networkMessage.ReadMessage<StringMessage>().value
        };

        if (networkState == NetworkState.Lobby)
        {
            playerController.statusText.text = "Your turn";
            playerController.ChangeCanvas("Character");
        }
        else if (networkState == NetworkState.InGame)
        {
            Debug.Log("Should get the turn");
            playerController.active = true;
            playerController.rolled = false;
        }
    }

    //Function to get available characters from the server.
    public void GetAvailableCharacters(NetworkMessage networkMessage)
    {
        StringMessage msg = new StringMessage
        {
            value = networkMessage.ReadMessage<StringMessage>().value
        };
        string[] deltas = msg.value.Split('|');

        playerController.SetCharacterButton(deltas);
    }

    //Function to get player primary info from the server.
    private void GetPlayerPrimaryInfo(NetworkMessage networkMessage)
    {
        StringMessage msg = new StringMessage
        {
            value = networkMessage.ReadMessage<StringMessage>().value
        };

        string[] deltas = msg.value.Split('|');

        playerController.SetPlayerPrimaryInfo(deltas[0], deltas[1], deltas[2]);
    }

    //Function to get player game info from the server.
    private void GetPlayerGameInfo(NetworkMessage networkMessage)
    {
        StringMessage msg = new StringMessage
        {
            value = networkMessage.ReadMessage<StringMessage>().value
        };

        string[] deltas = msg.value.Split('|');

        playerController.SetPlayerGameInfo(deltas[0], deltas[1], deltas[2]);
    }

    //Function to get player skill info from the server.
    private void GetPlayerSkillInfos(NetworkMessage networkMessage)
    {
        StringMessage msg = new StringMessage
        {
            value = networkMessage.ReadMessage<StringMessage>().value
        };

        string[] deltas = msg.value.Split('|');

        playerController.SetPlayerSkillInfos(deltas);
    }

    //Function to get the checkpoint options from the server.
    private void GetCheckPointOptions(NetworkMessage networkMessage)
    {
        StringMessage msg = new StringMessage
        {
            value = networkMessage.ReadMessage<StringMessage>().value
        };
        string[] deltas = msg.value.Split('|');

        playerController.SetAvailableTransitionChoice(deltas);
    }

    //Function to get the target players from the server.
    private void GetTargetPlayers(NetworkMessage networkMessage)
    {
        StringMessage msg = new StringMessage
        {
            value = networkMessage.ReadMessage<StringMessage>().value
        };
        string[] deltas = msg.value.Split('|');

        if (msg.value == "")
        {
            return;
        }

        playerController.SetTargets(deltas);
    }

    //Funtion to send the character selected by the network player controller.
    public void SendSelectedCharacter(string characterName)
    {
        StringMessage msg = new StringMessage();
        msg.value = characterName.ToString();

        client.Send(222, msg);
    }

    //Function to send the roll value to the client.
    public void SendRollValue(int rollValue)
    {
        StringMessage msg = new StringMessage();
        msg.value = rollValue.ToString();

        client.Send(333, msg);
    }

    //Function to send the player map transition choice to the player.
    public void SendMapTransitionChoice(string choice)
    {
        StringMessage msg = new StringMessage();
        msg.value = choice;
        client.Send(444, msg);
    }

    //Function to send the player decision to end turn to the server.
    public void SendEndTurn(string turnEnder)
    {
        StringMessage msg = new StringMessage();
        msg.value = turnEnder;
        client.Send(555, msg);
    }

    //Function to send the skill casted by the network player controller.
    public void SendCastedSkill(string skillName, string targetName)
    {
        StringMessage msg = new StringMessage();
        msg.value = skillName + "|" + targetName;
        client.Send(666, msg);
    }

    //Function to ask for targets from the server.
    public void AskForTargets(string skill)
    {
        StringMessage msg = new StringMessage();
        msg.value = Network.player.ipAddress + "|" + skill;
        client.Send(777, msg);
    }
}
