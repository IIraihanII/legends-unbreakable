﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;

public class ServerManager : MonoBehaviour
{
    //Used to decidie how the server manager treats clients.
    public enum NetworkState
    {
        MatchMaking,
        Lobby,
        InGame
    }

    public NetworkState networkState;

    string ipvSTR = "::ffff:";

    //Reference to the game manager.
    GameManager gameManager;

    //List to contain ip addresses of the connected clients.
    public List<String> connectedClientsAddresses = new List<String>(0);
    //List to contain client that have connected.
    private List<NetworkConnection> clientConnections = new List<NetworkConnection>(0);

    //return a client connection based on the ipaddress provided.
    public NetworkConnection GetClientConnection(string ipaddress)
    {
        foreach (NetworkConnection client in clientConnections)
        {
            if (client.address == ipaddress)
            {
                return client;
            }
        }

        return null;
    }

    //GUI drawn to check stats for test purposes.
    private void OnGUI()
    {
        string ipaddress = Network.player.ipAddress;
        GUI.Box(new Rect(10, Screen.height - 50, 100, 50), ipaddress);
        GUI.Label(new Rect(20, Screen.height - 35, 100, 20), "Status: " + NetworkServer.active);
        GUI.Label(new Rect(20, Screen.height - 20, 100, 20), "Connected: " + clientConnections.Count);
    }

    void Awake()
    {
        DontDestroyOnLoad(gameObject);

        networkState = NetworkState.MatchMaking;
        NetworkServer.Listen(25000);

        //Handle funtions for the server manager.
        NetworkServer.RegisterHandler(111, GetPlayerName);
        NetworkServer.RegisterHandler(222, GetSelectedCharacter);
        NetworkServer.RegisterHandler(333, GetPlayerRollValue);
        NetworkServer.RegisterHandler(666, GetCastedSkill);
        NetworkServer.RegisterHandler(777, GetRequestForTargets);
        NetworkServer.RegisterHandler(444, GetCheckPointSelection);
        NetworkServer.RegisterHandler(555, GetEndOfTurn);
    }

    private void Start()
    {
        gameManager = GetComponent<GameManager>();
    }

    void Update()
    {
        //Server behavior during match making.
        if (networkState == NetworkState.MatchMaking)
        {
            Button startGameButton = GameObject.Find("StarGame").GetComponent<Button>();

            if (clientConnections.Count == 0)
            {
                startGameButton.interactable = false;
            }
            else
            {
                startGameButton.interactable = true;
            }

            Text ipText = GameObject.Find("IP").GetComponent<Text>();
            Text connectionCountText = GameObject.Find("ConnectedClients").GetComponent<Text>();
            ipText.text = Network.player.ipAddress;
            connectionCountText.text = clientConnections.Count.ToString();

            AddClient();
            RemoveClient();
        }
        //Server behavior during in game session.
        else if (networkState == NetworkState.InGame)
        {
            ReconnectClient();
        }
        //Server behavior during lobby mode.
        else if (networkState == NetworkState.Lobby)
        {
            AddClient();
            RemoveClient();
        }
    }

    //Function to add client to the game.
    void AddClient()
    {
        foreach (NetworkConnection connection in NetworkServer.connections)
        {
            if (connection != null && !clientConnections.Contains(connection))
            {
                if (connectedClientsAddresses.Contains(connection.address) && networkState == NetworkState.InGame)
                {
                    ReconnectClient();
                }

                clientConnections.Add(connection);
                connectedClientsAddresses.Add(connection.address);
            }
        }
    }

    //Function to remove client from the game.
    void RemoveClient()
    {
        foreach (string connectionAddress in connectedClientsAddresses)
        {
            if (clientConnections.Any(p => p.address == connectionAddress))
            {
                //case for removing the player here.
            }
        }
    }

    //Function to reconnect the client if its of a known ip address.
    void ReconnectClient()
    {
        //Case for reconnect
    }


    //Function to get player name from the client.
    void GetPlayerName(NetworkMessage networkMessage)
    {
        if (networkState == NetworkState.Lobby)
        {
            StringMessage msg = new StringMessage
            {
                value = networkMessage.ReadMessage<StringMessage>().value
            };
            string[] deltas = msg.value.Split('|');

            gameManager.AddPlayerToGame(deltas[0], ipvSTR + deltas[1]);

            msg.value = "Wait";

            GetClientConnection(ipvSTR + deltas[1]).Send(555, msg);
        }
    }

    //Function to get the player's selected character from the client.
    void GetSelectedCharacter(NetworkMessage networkMessage)
    {
        StringMessage msg = new StringMessage
        {
            value = networkMessage.ReadMessage<StringMessage>().value
        };

        gameManager.SetCharacter(msg.value);
    }

    //Function to get the player's roll value from the client.
    void GetPlayerRollValue(NetworkMessage networkMessage)
    {
        StringMessage msg = new StringMessage
        {
            value = networkMessage.ReadMessage<StringMessage>().value
        };

        gameManager.activePlayer.map.MovePlayer(gameManager.activePlayer, int.Parse(msg.value));
    }

    //Function to get the checkpoint route selected by the player.
    public void GetCheckPointSelection(NetworkMessage networkMessage)
    {
        StringMessage msg = new StringMessage
        {
            value = networkMessage.ReadMessage<StringMessage>().value
        };

        gameManager.MovePlayerToNewMap(msg.value);
    }

    //Get request from the client regarding target request.
    private void GetRequestForTargets(NetworkMessage networkMessage)
    {
        StringMessage msg = new StringMessage
        {
            value = networkMessage.ReadMessage<StringMessage>().value
        };

        string[] deltas = msg.value.Split('|');

        SendTargetPlayers(deltas[0], deltas[1]);
    }

    //Get information regarding the skill casted by the client.
    private void GetCastedSkill(NetworkMessage networkMessage)
    {
        StringMessage msg = new StringMessage
        {
            value = networkMessage.ReadMessage<StringMessage>().value
        };

        string[] deltas = msg.value.Split('|');

        gameManager.CastPlayerSkill(deltas[0], deltas[1]);
    }


    //Get the clients call to end their turn.
    private void GetEndOfTurn(NetworkMessage networkMessage)
    {
        StringMessage msg = new StringMessage
        {
            value = networkMessage.ReadMessage<StringMessage>().value
        };

        gameManager.SetActivePlayer();
        gameManager.UpdateAllSkillCooldowns();
    }

    //Function to change the current canvas visible on the client.
    public void ChangeClientCanvas(string canvaMSG)
    {
        foreach (NetworkConnection connection in clientConnections)
        {
            StringMessage msg = new StringMessage();
            msg.value = canvaMSG;
            connection.Send(555, msg);
        }
    }

    //Function to send the character availble for selection to the client.
    public void SendAvailableCharacters(string characterAvailability)
    {
        foreach (NetworkConnection connection in clientConnections)
        {
            StringMessage msg = new StringMessage();
            msg.value = characterAvailability;
            connection.Send(444, msg);
        }
    }

    //Function to send check point transition information to the client end.
    public void SendCheckPointOptions(Player player, string options)
    {
        StringMessage msg = new StringMessage();
        msg.value = options;
        GetClientConnection(player.ipAddress).Send(999, msg);
    }


    //Send players as targets to the client.
    public void SendTargetPlayers(string playerIP, string skillName)
    {
        StringMessage msg = new StringMessage();
        msg.value = gameManager.GetPlayersAsTargets(skillName);

        GetClientConnection(gameManager.activePlayer.ipAddress).Send(222, msg);
    }

    //Function to update the primary player info at the client end.
    public void UpdateClientPlayerPrimaryInfo(Player player)
    {
        StringMessage stringMessage = new StringMessage();
        stringMessage = player.GetPlayerDetails();

        GetClientConnection(player.ipAddress).Send(777, stringMessage);
    }

    //Function to update the player game info at the client end.
    public void UpdateClientPlayerGameInfo(Player player)
    {
        StringMessage stringMessage = new StringMessage();
        stringMessage = player.GetPlayerGameDetails();


        GetClientConnection(player.ipAddress).Send(888, stringMessage);
    }

    //Function to update the players skill info at the client end.
    public void UpdateClientPlayerSkillInfos(Player player)
    {
        StringMessage stringMessage = new StringMessage();
        stringMessage = player.GetNPlayerSkillDetails();

        GetClientConnection(player.ipAddress).Send(333, stringMessage);
    }
}
