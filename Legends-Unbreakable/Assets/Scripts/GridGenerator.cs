﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridGenerator : MonoBehaviour
{
    public GameObject hexPreFab;

    //Size of map interms of hex tiles
    public int width = 20;
    public int height = 20;

    float xOffSet = .5f;
    float zOffSet = .85f;

    private void Start()
    {
        for (int x = 0; x < width; x ++)
        {
            for (int y = 0; y < height; y ++)
            {
                float xPos = x;

                if (y % 2 == 0)
                {
                    xPos += xOffSet;
                }
                Instantiate(hexPreFab, new Vector3(xPos, 0, y * zOffSet), Quaternion.identity);
            }
        }
    }

}
