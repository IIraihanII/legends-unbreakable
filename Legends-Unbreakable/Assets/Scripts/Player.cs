﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Networking.NetworkSystem;

public class Player : LivingEntity
{
    //Reference to the game manager.
    GameManager gameManager;

    //Primary attributes.
    public string playerName;
    public int playerNumber;
    public bool isActive;
    public string ipAddress;

    bool isMoving;


    //Character attributes.
    private Character character;
    private CharacterAnimator characterAnimator;

    //Map attributes.
    public Map map;
    public Tile tile;

    //Skill attributes.
    public Skill[] skills = new Skill[3];

    //Navigation agent attributes.
    NavMeshAgent navMeshAgent;

    //Function to get the player currents character class.
    public string GetCharacterClass()
    {
        if (character != null)
        {
            return character.@class.ToString();
        }

        return "";
    }

    //Function to get the player current skill information.
    public Skill GetSkill(string skillName)
    {
        foreach (Skill skill in skills)
            if (skill.skillName == skillName)
                return skill;

        return null;
    }

    //Function to get the set a character to the player.
    public void SetCharacter(Character character)
    {
        this.character = character;
        characterAnimator = character.GetComponent<CharacterAnimator>();
    }

    //Function to Get the player primary  details.
    public StringMessage GetPlayerDetails()
    {
        StringMessage msg = new StringMessage();

        string playerPrimaryDetails = playerName + "|" + playerNumber + "|" + GetCharacterClass();

        msg.value = playerPrimaryDetails;

        return msg;
    }

    //Function to Get the player game details.
    public StringMessage GetPlayerGameDetails()
    {
        StringMessage msg = new StringMessage();

        string playerGameDetails = GetHP() + "|" + GetMP() + "|" + GetEXP();

        msg.value = playerGameDetails;

        return msg;
    }

    //Function to Get the player skill details.
    public StringMessage GetNPlayerSkillDetails()
    {
        StringMessage msg = new StringMessage();

        string playerSkillDetails = skills[0].GetSkillDetails() + "|" + skills[1].GetSkillDetails() + "|" + skills[2].GetSkillDetails();

        msg.value = playerSkillDetails;

        return msg;
    }

    //Function to Get the player as a target.
    public string GetPlayerAsTarget()
    {
        if (map != null)
        {
            string playerAsTarget = GetCharacterClass() + "|" + map.mapNumber + "|" + tile.tileIndex;
            return playerAsTarget;
        }
        else
        {
            return null;
        }
    }
    
    //Function to set player game details.
    public void SetPlayerGameDetails(StringMessage playerPrimaryDetails)
    {
        string[] deltas = playerPrimaryDetails.value.Split('|');

        SetHP(int.Parse(deltas[0]));
        SetMP(int.Parse(deltas[1]));
        AddEXP(int.Parse(deltas[2]));
    }

    void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

    void Start()
    {
        gameManager = FindObjectOfType<GameManager>();
        navMeshAgent = GetComponent<NavMeshAgent>();
    }

    void Update()
    {
        if (isMoving)
        {
            CheckDestinationReached();
        }

        if (GetHP() <= 0 && character != null)
        {
            Reset();
            StartCoroutine(Respawn(1f));
        }

        if (GetEXP() >= 80)
        {
            LevelUp();
            Reset();
            ResetEXP();

            character.GainEffect();
            gameManager.UpdateAllPlayerInfo();
        }
    }

    //Teleport player to a cetain map position.
    public void TeleportMove(Map map, Tile tile)
    {
        this.map = map;
        this.tile = tile;

        transform.position = tile.RestAt().position;

        navMeshAgent.SetDestination(transform.position);

        character.GainEffect();
    }

    //Make player move to a new position.
    public void MoveTo(Map map, Tile tile)
    {
        this.tile.Leave();

        this.map = map;
        this.tile = tile;

        Vector3 targetDestination = tile.RestAt().position;

        navMeshAgent.SetDestination(new Vector3(targetDestination.x, transform.position.y, targetDestination.z));

        isMoving = true;
    }

    //Check if player has reached destination.
    void CheckDestinationReached()
    {
        if (!navMeshAgent.pathPending)
        {
            if (navMeshAgent.remainingDistance <= navMeshAgent.stoppingDistance)
            {
                if (!navMeshAgent.hasPath || navMeshAgent.velocity.sqrMagnitude == 0f)
                {
                    Debug.Log("At destination");

                    CheckTileEventOnArrival();
                    isMoving = false;
                }
            }
        }
    }

    //Get checkpoint routes from the tile the player is on.
    public string GetCheckpointRoutes(Map[] maps)
    {
        string mapRoutes = "";

        foreach (Map map in maps)
        {
            mapRoutes = mapRoutes + "|" + map.name;
        }

        mapRoutes = mapRoutes.Substring(1);

        return mapRoutes;
    }

    //Check what kind of tile player arrived on.
    void CheckTileEventOnArrival()
    {
        switch (tile.tileType)
        {
            case Tile.TileType.Regular:
                AddEXP(10);
                break;
            case Tile.TileType.CheckPoint:
                Reset();
                gameManager.SendCheckPointRoutes(GetCheckpointRoutes(tile.transitionMaps));
                gameManager.UpdateAllPlayerInfo();
                return;
        }
    }

    //Function to cast a skill.
    public void CastSkill(string skillName, Player[] targets)
    {
        for (int i = 0; i < skills.Length; i++)
        {
            if (skills[i].skillName == skillName)
            {
                skills[i].Cast(targets);
                switch (skills[i].skillType)
                {
                    case Skill.SkillType.Offensive:
                        transform.LookAt(targets[0].transform);
                        characterAnimator.Attack();
                        targets[0].characterAnimator.TakeDamage();
                        character.SkillEffect(i, skills[i].skillType, targets);
                        break;
                    case Skill.SkillType.Heal:
                        character.SkillEffect(i, skills[i].skillType, targets);
                        break;
                    case Skill.SkillType.Multi:
                        character.SkillEffect(i, skills[i].skillType, targets);
                        characterAnimator.Attack();
                        foreach (Player targetPlayer in targets)
                        {
                            targetPlayer.characterAnimator.TakeDamage();
                        }
                        break;
                }

                ReduceMP(skills[i].skillCost);
            }
        }
    }

    //Update the cool down for all the skills.
    public void UpdateSkillCoolDowns()
    {
        foreach (Skill skill in skills)
        {
            skill.UpdateSkillCoolDown();
        }
    }

    //Level up the player.
    public void LevelUp()
    {
        switch (character.@class)
        {
            case Character.Class.Warrior:
                foreach (Skill skill in skills)
                {
                    skill.UpgradeSkill(1, 3);
                }
                break;
            case Character.Class.Archer:
                foreach (Skill skill in skills)
                {
                    skill.UpgradeSkill(0, 5);
                }
                break;
            case Character.Class.Caster:
                foreach (Skill skill in skills)
                {
                    skill.UpgradeSkill(1, 8);
                }
                break;
        }
    }

    //Start the navigation mesh for the player.
    public void StartNavMesh()
    {
        GetComponent<NavMeshAgent>().enabled = true;
    }

    //Function to indicate player has died.
    public void Death()
    {
        map.AddPlayerToMap(GetComponent<Player>());
        gameManager.UpdateAllPlayerInfo();
    }

    public void Win()
    {
        gameManager.DeclareWinner();
    }

    //Enumerator to respwan the player after they died.
    IEnumerator Respawn(float time)
    {
        characterAnimator.Die();
        character.DeathEffect();

        yield return new WaitForSeconds(time);

        Death();
    }
}
