﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LobbyMenu : CanvasManager
{
    public CanvasGroup lobbyCanvas;
    public CanvasGroup characterSelectCanvas;

    public Text[] playerNames;

    public Text[] altPlayerNames;

    public Text hostAddress;

    public Text hostMessages;

    public Text charSelect;

    public Text lobbyMessage;

    public bool[] charactersAvailable;

    public string GetAvailableCharacter()
    {
        string availability = "";

        foreach (bool available in charactersAvailable)
        {
            availability = availability + "|" +available;
        }

        availability = availability.Substring(1);

        return availability;
    }

    public void UpdateAvailableCharacters(string character)
    {
        switch (character)
        {
            case "Warrior":
                charactersAvailable[0] = false;
                break;
            case "Caster":
                charactersAvailable[1] = false;
                break;
            case "Archer":
                charactersAvailable[2] = false;
                break;
        }
    }

    void Start ()
    {
        charactersAvailable = new bool[playerNames.Length];
        for (int i = 0; i < charactersAvailable.Length; i++)
        {
            charactersAvailable[i] = true;
        }
	}

    public void AddPlayerName(string _name)
    {
        for (int i = 0; i < playerNames.Length; i++)
        {
            if (playerNames[i].text == "")
            {
                playerNames[i].text = _name;

                lobbyMessage.text = "Player " + _name + " has joined the game.";

                break;
            }
        }
    }

    public void AddPlayerNameToCharacter(string name, string characterType)
    {
        for (int i = 0; i < altPlayerNames.Length; i++)
        {
            if (altPlayerNames[i].name == characterType)
            {
                altPlayerNames[i].text = name;
            }
        }
    }

    public void PresentCharacters()
    {
        ShowCanvas(lobbyCanvas, characterSelectCanvas);
    }
}
