﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking.NetworkSystem;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    //Prefab to instantiate new player.
    [SerializeField]
    private Player playerPrefab;
    //Player that is set to be active.
    public Player activePlayer;

    //BGM Audio.
    AudioSource audioSource;

    //List containting players involved in the game.
    private List<Player> players = new List<Player>(0);

    //Reference to the server manager.
    ServerManager serverManager;

    void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

    void Start()
    {
        serverManager = GetComponent<ServerManager>();
        audioSource = GetComponent<AudioSource>();
        audioSource.Stop();
    }

    private void OnEnable() { SceneManager.sceneLoaded += OnSceneLoaded; }

    private void OnDisable() { SceneManager.sceneLoaded -= OnSceneLoaded; }

    [SerializeField]
    GameObject showWinnerCanvas;
    [SerializeField]
    Text winnerText;
    [SerializeField]
    Button quitButton;

    //Function to check which scene has been loaded.
    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if (scene.name == "EnchantedForest")
        {
            Map map = GameObject.Find("SegmentA").GetComponent<Map>();

            foreach (Player player in players)
            {
                map.AddPlayerToMap(player);
            }

            SetActivePlayer();

            StartCoroutine(SetUpWinnerCanvas());
        }
    }

    //Add player with primary details.
    public void AddPlayerToGame(string playerName, string ipAddress)
    {
        LobbyMenu lobby = FindObjectOfType<LobbyMenu>();

        Player player;

        player = Instantiate(playerPrefab);
        player.playerName = playerName;
        player.ipAddress = ipAddress;

        player.playerNumber = players.Count;

        players.Add(player);

        FindObjectOfType<LobbyMenu>().AddPlayerName(playerName);

        if (players.Count == serverManager.connectedClientsAddresses.Count)
        {
            lobby.lobbyMessage.text = "Select your Character";
            serverManager.SendAvailableCharacters(lobby.GetAvailableCharacter());
            SetActivePlayer();
        }

    }

    //Notify server manager to initiate character select on client.
    public void InitiateCharacterSelect()
    {
        serverManager.networkState = ServerManager.NetworkState.Lobby;
        serverManager.ChangeClientCanvas("EnterName");
        SceneManager.LoadScene("Lobby");
    }

    //Set character to a player.
    public void SetCharacter(string characterName)
    {
        LobbyMenu lobby = FindObjectOfType<LobbyMenu>();

        Character character = GameObject.Find(characterName).GetComponent<Character>();

        activePlayer.transform.position = character.transform.position;
        activePlayer.transform.rotation = character.transform.rotation;

        character.transform.SetParent(activePlayer.transform);
        activePlayer.SetCharacter(character);

        character.SetCharacterAttributes();

        lobby.UpdateAvailableCharacters(characterName);

        lobby.AddPlayerNameToCharacter(activePlayer.playerName, activePlayer.GetCharacterClass());

        serverManager.SendAvailableCharacters(FindObjectOfType<LobbyMenu>().GetAvailableCharacter());
        SetActivePlayer();

        if (!players.Any(p => p.maxHP == 0))
        {
            lobby.PresentCharacters();

            StartCoroutine(InitiateGame(2f));
        }
    }

    //Set active player conditionally.
    public void SetActivePlayer()
    {
        int index = 0;

        if (players[players.Count - 1].isActive == true)
            index = 0;
        else if (players.Count == 1)
            index = 0;
        else
            index++;

        foreach (Player player in players)
        {
            player.isActive = false;
        }
        players[index].isActive = true;
        activePlayer = players[index];

        if (serverManager.networkState == ServerManager.NetworkState.Lobby 
            || serverManager.networkState == ServerManager.NetworkState.InGame)
        {
            StringMessage msg = new StringMessage();
            msg.value = "Active";

            serverManager.GetClientConnection(players[index].ipAddress).Send(666, msg);

            if (GameObject.FindGameObjectWithTag("FollowCamera"))
            {
                CameraFollow cameraFollow = GameObject.FindGameObjectWithTag("FollowCamera").GetComponent<CameraFollow>();
                cameraFollow.target = activePlayer.transform;
            }
        }

        UpdateAllPlayerInfo();
    }

    //Return checkpoint routes to active player.
    public void SendCheckPointRoutes(string availableRoutes)
    {
        Debug.Log(availableRoutes + " transition info being sent");

        serverManager.SendCheckPointOptions(activePlayer, availableRoutes);
    }

    //Update information for all players.
    public void UpdateAllPlayerInfo()
    {
        foreach (Player player in players)
        {
            serverManager.UpdateClientPlayerPrimaryInfo(player);
            serverManager.UpdateClientPlayerGameInfo(player);
            serverManager.UpdateClientPlayerSkillInfos(player);
        }
    }

    //Update the cooldown value of all the player's skill.
    public void UpdateAllSkillCooldowns()
    {
        foreach (Player player in players)
        {
            player.UpdateSkillCoolDowns();
        }
    }

    //Transport player to new map segment.
    public void MovePlayerToNewMap(string mapName)
    {
        Map map = GameObject.Find(mapName).GetComponent<Map>();

        Debug.Log("Player is to be added to new map " + map.name);

        map.AddPlayerToMap(activePlayer);
    }

    //Cast a player skill based on skill type
    public void CastPlayerSkill(string skillName, string skillTarget)
    {

        List<Player> targetPlayers = new List<Player>();

        if (skillTarget == "All")
        {
            foreach (Player player in players)
            {
                if (player != activePlayer)
                {
                    targetPlayers.Add(player);
                }
            }

            activePlayer.CastSkill(skillName, targetPlayers.ToArray());
        }
        else if (skillTarget == "Self")
        {
            targetPlayers.Add(activePlayer);
            activePlayer.CastSkill(skillName, targetPlayers.ToArray());
        }
        else
        {
            foreach (Player player in players)
            {
                if (player.GetCharacterClass() == skillTarget && player != activePlayer)
                {
                    targetPlayers.Add(player);
                    break;
                }
            }

            activePlayer.CastSkill(skillName, targetPlayers.ToArray());
        }

        UpdateAllPlayerInfo();
    }

    //Get target information for players.
    public string GetPlayersAsTargets(string skillName)
    {
        string targetPlayers = "";


        Skill castedSkill = activePlayer.GetSkill(skillName);

        foreach (Player player in players)
        {
            if (player != activePlayer && player.map.mapNumber == activePlayer.map.mapNumber)
            {
                if (Mathf.Abs(player.tile.tileIndex - activePlayer.tile.tileIndex) <= castedSkill.skillRange)
                {
                    targetPlayers = targetPlayers + player.GetCharacterClass() + "|";
                }
            }
        }
        return targetPlayers;
    }

    //Show Victor.
    public void DeclareWinner()
    {
        winnerText.text = activePlayer.playerName;
        showWinnerCanvas.gameObject.SetActive(true);

        serverManager.ChangeClientCanvas("EndGame");

        quitButton.onClick.AddListener(EndGame);
    }

    //End the game.
    public void EndGame()
    {
        Application.Quit();
    }


    //Initiate game with delay.
    public IEnumerator InitiateGame(float _time)
    {
        yield return new WaitForSeconds(_time);

        serverManager.networkState = ServerManager.NetworkState.InGame;

        UpdateAllPlayerInfo();

        serverManager.ChangeClientCanvas("InGame");

        SceneManager.LoadScene("EnchantedForest");
    }

    public IEnumerator SetUpWinnerCanvas()
    {
        yield return new WaitForSeconds(3f);

        showWinnerCanvas = GameObject.FindGameObjectWithTag("WinCanvas");
        winnerText = GameObject.FindGameObjectWithTag("WinnerText").GetComponent<Text>();

        quitButton = GameObject.FindGameObjectWithTag("QuitButton").GetComponent<Button>();

        showWinnerCanvas.gameObject.SetActive(false);

        audioSource.Play();
    }
}
