﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class NetworkPlayerController : CanvasManager
{
    [System.Serializable]
    class PlayerInfo
    {
        public string playerName;
        public int playerNumber;
        public string playerCharacter;

        public int playerHP;
        public int playerMP;


        public int gems;
    }

    [System.Serializable]
    class SkillInfo
    {
        public string skillType;

        public string skillName;
        public int skillCost;
        public int skillEffectValue;
        public int skillCoolDown;
        public int skillRange;
    }

    [System.Serializable]
    class Target
    {
        public string targetClass;
    }

    //Host address canvas.
    [SerializeField]
    CanvasGroup enterHostAddressCG;
    [SerializeField]
    InputField hostAddress;

    //Enter player name canvas. 
    [SerializeField]
    CanvasGroup enterPlayerDetailsCG;

    //Character select canvas.
    [SerializeField]
    CanvasGroup characterSelectCG;
    [SerializeField]
    Button[] characterButton;

    //Canvas for playing the game.
    [SerializeField]
    Image playerImage;

    public Text statusText;

    public Sprite warriorImage;
    public Sprite archerImage;
    public Sprite casterImage;

    [SerializeField]
    CanvasGroup inGameCG;
    [SerializeField]
    CanvasGroup skillsCG;
    [SerializeField]
    CanvasGroup targetsCG;
    [SerializeField]
    CanvasGroup mapTransitionCG;

    [SerializeField]
    Text playerName;

    [SerializeField]
    Text hpText;
    [SerializeField]
    Text mpText;
    [SerializeField]
    Text gemsText;

    [SerializeField]
    Button rollButton;
    [SerializeField]
    Button endTurnButton;

    [SerializeField]
    Button[] skillButton;
    [SerializeField]
    Text[] skillButtonText;

    [SerializeField]
    Button[] targetButtons;
    [SerializeField]
    Text[] targetButtonText;

    [SerializeField]
    Button[] mapTransitionButtons;
    [SerializeField]
    Text[] transitionButtonTexts;

    //Wait for host canvas
    [SerializeField]
    CanvasGroup waitScreenCG;
    [SerializeField]
    CanvasGroup endScreenCG;

    //SkillDescriptionInfo
    [SerializeField]
    Text[] skillNames;
    [SerializeField]
    Text[] skillCosts;
    [SerializeField]
    Text[] skillRanges;
    [SerializeField]
    Text[] skillEffects;


    //Controller Attributes
    ClientManager clientManager;
    public bool active = false;
    public bool rolled = false;
    public bool selectingRoute = false;

    [SerializeField]
    PlayerInfo playerInfo;
    [SerializeField]
    SkillInfo[] skillInfos;
    [SerializeField]
    Target[] targets;

    public string armedSkill;

    bool casting = false;

    void Start()
    {
        Screen.orientation = ScreenOrientation.Portrait;
        clientManager = GetComponent<ClientManager>();
    }

    void Update()
    {
        if (clientManager.networkState == ClientManager.NetworkState.InGame)
        {
            hpText.text = playerInfo.playerHP.ToString();
            mpText.text = playerInfo.playerMP.ToString();
            gemsText.text = playerInfo.gems.ToString();

            for (int i = 0; i < skillInfos.Length; i++)
            {
                if (skillInfos[i].skillCoolDown == 0 && playerInfo.playerMP >= skillInfos[i].skillCost)
                {
                    skillButtonText[i].text = skillInfos[i].skillName;
                    if (active)
                    {
                        skillButton[i].interactable = true;
                    }
                    else
                    {
                        skillButton[i].interactable = false;
                    }
                }
                else if (skillInfos[i].skillCoolDown > 0)
                {
                    skillButtonText[i].text = skillInfos[i].skillCoolDown.ToString();
                    skillButton[i].interactable = false;
                }
                else if (skillInfos[i].skillCost > playerInfo.playerMP)
                {
                    skillButtonText[i].text = "Short on :" + (skillInfos[i].skillCost - playerInfo.playerMP) + "MP";
                    skillButton[i].interactable = false;
                }
            }

            for (int i = 0; i < skillInfos.Length; i++)
            {
                skillNames[i].text = skillInfos[i].skillName;
                skillCosts[i].text = "Cost " + skillInfos[i].skillCost.ToString();
                if (skillInfos[i].skillRange == 0)
                {
                    skillRanges[i].text = "No range";
                }
                else
                {
                    skillRanges[i].text = "Range " + skillInfos[i].skillRange.ToString();
                }
                skillEffects[i].text = "Effect " + skillInfos[i].skillEffectValue.ToString();
            }

            switch (playerInfo.playerCharacter)
            {
                case "Warrior":
                    playerImage.sprite = warriorImage;
                    break;
                case "Caster":
                    playerImage.sprite = casterImage;
                    break;
                case "Archer":
                    playerImage.sprite = archerImage;
                    break;
            }
        }

        if (active && !selectingRoute)
        {
            endTurnButton.interactable = true;

            if (!rolled)
            {
                rollButton.interactable = true;
            }
            else
            {
                rollButton.interactable = false;
            }
        }
        else
        {
            endTurnButton.interactable = false;
            rollButton.interactable = false;
        }

        
    }

    //Connect to the game server.
    public void ConnectToServer(Button button)
    {
        if (!clientManager.GetConnectionStatus() && hostAddress.text != "")
            StartCoroutine(AttemptConnection(1f, button));
    }

    //Send in game name to the game server.
    public void SendPlayerName(InputField inputField)
    {
        if (inputField.text != "")
        {
            clientManager.SendPlayerName(inputField.text);
            ShowCanvas(enterPlayerDetailsCG, waitScreenCG);
        }
    }

    //Send selected character to the game server.
    public void SelectCharacter(Button button)
    {
        foreach (Button characterButton in characterButton)
            if (characterButton == button)
                clientManager.SendSelectedCharacter(button.name);
    }

    //Change canvas based on action name.
    public void ChangeCanvas(string message)
    {
        switch (message)
        {
            case "Wait":
                break;
            case "EndGame":
                ShowCanvas(inGameCG, endScreenCG);
                break;
            case "EnterName":
                ShowCanvas(waitScreenCG, enterPlayerDetailsCG);
                break;
            case "Character":
                ShowCanvas(waitScreenCG, characterSelectCG);
                break;
            case "InGame":
                ShowCanvas(characterSelectCG, inGameCG);
                active = false;
                break;
        }
    }

    //Disable character buttons based on whats selected already.
    public void SetCharacterButton(string[] characterAvailability)
    {
        for (int i = 0; i < characterAvailability.Length; i++)
            characterButton[i].interactable = System.Convert.ToBoolean(characterAvailability[i]);
    }


    //Code below is for the in game interface.
    public void SetPlayerPrimaryInfo(string playerName, string playerNumber, string playerClass)
    {
        playerInfo.playerName = playerName;
        playerInfo.playerNumber = int.Parse(playerNumber);
        playerInfo.playerCharacter = playerClass;

        if (playerClass != "")
            clientManager.networkState = ClientManager.NetworkState.InGame;

        this.playerName.text = playerName;
    }

    public void SetPlayerGameInfo(string playerHP, string playerMP, string gems)
    {
        playerInfo.playerHP = int.Parse(playerHP);
        playerInfo.playerMP = int.Parse(playerMP);
        playerInfo.gems = int.Parse(gems);
    }

    public void SetPlayerSkillInfos(string[] skillInfos)
    {
        int index = 0;

        for (int i = 0; i < this.skillInfos.Length; i++)
        {
            this.skillInfos[i].skillName = skillInfos[index];
            this.skillInfos[i].skillCost = int.Parse(skillInfos[index + 1]);
            this.skillInfos[i].skillEffectValue = int.Parse(skillInfos[index + 2]);
            this.skillInfos[i].skillCoolDown = int.Parse(skillInfos[index + 3]);
            this.skillInfos[i].skillType = skillInfos[index + 4];
            this.skillInfos[i].skillRange = int.Parse(skillInfos[index + 5]);

            index += 6;
        }
    }

    public void SetTargets(string[] targetsStr)
    {
        int index = 0;

        for (int i = 0; i < targets.Length; i++)
            if (targetsStr[i] != "")
            {
                targets[i].targetClass = targetsStr[index];
                targetButtonText[i].text = targetsStr[index];
                targetButtons[i].gameObject.SetActive(true);
            }
    }

    public void SetAvailableTransitionChoice(string[] choices)
    {
        for (int i = 0; i < choices.Length; i++)
        {
            if (choices[i] != "")
            {
                mapTransitionButtons[i].gameObject.SetActive(true);
                transitionButtonTexts[i].text = choices[i];

                ShowCanvas(skillsCG, mapTransitionCG);
            }
        }

        selectingRoute = true;
    }

    public void SelectTransition(Text text)
    {
        foreach (Button transitionButton in mapTransitionButtons)
        {
            transitionButton.gameObject.SetActive(false);
            clientManager.SendMapTransitionChoice(text.text);
        }

        ShowCanvas(mapTransitionCG, skillsCG);

        selectingRoute = false;
    }

    public void Roll()
    {
        int rollValue = Random.Range(1, 3);
        rolled = true;
        StartCoroutine(SendRollValue(.5f, rollValue));
    }

    public void EndTurn()
    {
        statusText.text = "Your Turn Ended";

        clientManager.SendEndTurn("End");
        active = false;
    }

    public void ShowTargets(Button skillButton)
    {
        clientManager.AskForTargets(skillButton.GetComponentInChildren<Text>().text);
        armedSkill = skillButton.GetComponentInChildren<Text>().text;

        foreach (SkillInfo skill in skillInfos)
        {
            if (armedSkill == skill.skillName)
            {
                if (skill.skillType == "Multi")
                {
                    Debug.Log("Multi type casted");
                    clientManager.SendCastedSkill(armedSkill, "All");
                    return;
                }
                else if (skill.skillType == "Heal")
                {
                    Debug.Log("Healing type casted");
                    clientManager.SendCastedSkill(armedSkill, "Self");
                    return;
                }
            }
        }

        Debug.Log("Attack type casted");

        StartCoroutine(WaitForTargetInfo(2f));
    }

    public void CastSkill(Button button)
    {
        foreach (Target target in targets)
        {
            string text = button.GetComponentInChildren<Text>().text;

            if (target.targetClass == text)
            {
                clientManager.SendCastedSkill(armedSkill, target.targetClass);
                button.gameObject.SetActive(false);
                ShowCanvas(targetsCG, skillsCG);
                return;
            }
        }
    }

    public void CancleCast()
    {
        armedSkill = "";
        ShowCanvas(targetsCG, skillsCG);
    }

    //Wait for a specific time to see if connection was made to the game server.
    IEnumerator AttemptConnection(float time, Button button)
    {
        button.interactable = false;

        clientManager.ConnectToServer(hostAddress.text);

        yield return new WaitForSeconds(time);

        if (clientManager.GetConnectionStatus())
        {
            clientManager.networkState = ClientManager.NetworkState.Lobby;

            ShowCanvas(enterHostAddressCG, waitScreenCG);
        }
        else
        {
            button.interactable = true;
        }
    }

    IEnumerator SendRollValue(float time, int rollValue)
    {
        yield return new WaitForSeconds(time);

        statusText.text = "You Rolled " + rollValue;

        clientManager.SendRollValue(rollValue);
    }

    IEnumerator WaitForTargetInfo(float time)
    {
        yield return new WaitForSeconds(time);

        ShowCanvas(skillsCG, targetsCG);
    }
}
