﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialougeManager : MonoBehaviour
{
    public Text nameText;
    public Text dialougeText;

    CanvasManager canvasManager;

    public Queue<string> sentences;

    private void Start()
    {
        canvasManager = GetComponent<CanvasManager>();
        sentences = new Queue<string>();
    }

    public void StartDialouge(Dialouge dialouge)
    {
        nameText.text = dialouge.name;
        sentences.Clear();

        foreach (string sentence in dialouge.sentences)
        {
            sentences.Enqueue(sentence);
        }

        DisplayNextSentence();
    }

    public void DisplayNextSentence()
    {
        if (sentences.Count == 0)
        {
            return;
        }

        string sentence = sentences.Dequeue();
        dialougeText.text = sentence;
    }

    public void EnableDialougeEnder(Button button)
    {
        if (sentences.Count == 0)
        {
            button.gameObject.SetActive(true);
        }
    }

    public void DisableDialougeSwitcher(Button button)
    {
        if (sentences.Count == 0)
        {
            button.gameObject.SetActive(false);
        }
    }
}
