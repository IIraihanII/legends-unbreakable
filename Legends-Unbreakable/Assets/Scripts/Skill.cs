﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Skill
{
    public enum SkillType
    {
        Offensive,
        Defensive,
        Heal,
        Multi,
        Disable
    }

    public SkillType skillType;
    public string skillName;
    public int skillCurrentCoolDown;
    public int skillCoolDown;
    public int skillEffectValue;
    public int skillCost;
    public int skillRange;

    public void SetSkill(SkillType skillType, string skillName, int skillCoolDown ,int skillEffectValue, int skillCost, int skillRange)
    {
        this.skillType = skillType;
        this.skillName = skillName;
        this.skillCoolDown = skillCoolDown;
        this.skillEffectValue = skillEffectValue;
        this.skillCost = skillCost;
        this.skillRange = skillRange;
    }

    public void UpdateSkillCoolDown()
    {
        skillCurrentCoolDown--;

        if (skillCurrentCoolDown == -1)
        {
            skillCurrentCoolDown = 0;
        }
    }

    public void UpgradeSkill(int skillCoolDown, int skillEffectValue)
    {
        this.skillCoolDown -= skillCoolDown;
        this.skillEffectValue += skillEffectValue;
    }

    public string GetSkillDetails()
    {
        return skillName + "|" + skillCost + "|" + skillEffectValue + "|" + skillCurrentCoolDown + "|" + skillType.ToString() + "|" + skillRange.ToString();
    }

    public void Cast(Player[] targets)
    {
        foreach (Player target in targets)
        {
            switch (skillType)
            {
                case SkillType.Offensive:
                    target.TakeDamage(skillEffectValue);
                    Debug.Log("Casting is damaging");
                    break;
                case SkillType.Defensive:
                    target.AddArmour(skillEffectValue);
                    break;
                case SkillType.Multi:
                    Debug.Log("Casting is damaging all");
                    target.TakeDamage(skillEffectValue);
                    break;
                case SkillType.Heal:
                    Debug.Log("Casting is healing");
                    target.Heal(skillEffectValue);
                    break;
            }
        }
        
        skillCurrentCoolDown = skillCoolDown;
    }
}
