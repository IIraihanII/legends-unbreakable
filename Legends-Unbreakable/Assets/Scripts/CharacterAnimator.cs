﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CharacterAnimator : MonoBehaviour {

    public NavMeshAgent agent;
    Animator animator;

    bool hasPrimary = false;

    private void Start()
    {
        animator = GetComponentInChildren<Animator>();
    }

    void Update ()
    {
        if (GetComponentInParent<Character>() && !hasPrimary)
        {
            hasPrimary = true;
        }

        if (hasPrimary)
        {
            agent = GetComponentInParent<NavMeshAgent>();
        }

        if (agent != null)
        {
            Debug.Log("Should be animating");
            float speedPercentage = agent.velocity.magnitude / agent.speed;
            animator.SetFloat("speed", speedPercentage, .1f, Time.deltaTime);
        }

    }

    public void TakeDamage()
    {
        animator.SetTrigger("takeDamage");
    }

    public void Attack()
    {
        animator.SetTrigger("attack");
    }

    public void Die()
    {
        animator.SetTrigger("die");
    }

}
