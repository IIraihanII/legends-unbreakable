﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnedParticle : MonoBehaviour
{

	void Start ()
    {
        StartCoroutine(DelayedDestroy(1f));
	}
	
	IEnumerator DelayedDestroy(float time)
    {
        yield return new WaitForSeconds(time);

        Destroy(gameObject);
    }
}
